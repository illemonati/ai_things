import gym
import numpy as np
import time

"""setup"""
env = gym.make("Taxi-v2")
state = env.reset()
EPISODES_YOU_GOT_TO = 621000
in_file = open("out-{}-episodes".format(EPISODES_YOU_GOT_TO), "rb")
q_table = np.load(in_file)
"""end_setup"""


if __name__ == '__main__':
    done = False
    reward_total = 0
    state = env.reset()
    time_step = 0
    reward = 0
    #do thing every time step
    while not done:
        env.render()
        print('''Reward: {}\n
               Reward Total: {}
               State: {}\n
               Time Step: {}\n
               '''.format(reward, reward_total, state, time_step))

        action = np.argmax(q_table[state])

        next_state, reward, done, info = env.step(action)
        state = next_state
        time_step += 1
        reward_total += reward
        time.sleep(0.5)

    env.render()
    print('''Reward: {}\n
           Reward Total: {}
           State: {}\n
           Time Step: {}\n
           '''.format(reward, reward_total, state, time_step))


    in_file.close()

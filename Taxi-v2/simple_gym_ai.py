import gym
import numpy as np
import random
import os
# import cupy as cp

"""setup"""
env = gym.make("Taxi-v2")
state = env.reset()
states_possible = env.observation_space.n
actions_possible = env.action_space.n
q_table = np.zeros([states_possible, actions_possible])
EPISODES = range(0,9999999999999)
epsilon = 0.9
epsilon_min = 0.01
epsilon_decay = 0.005
alpha = 0.618
# CUDA = ON
"""end_setup"""


for episode in EPISODES:
    done = False
    reward = 0
    state = env.reset()
    #do thing every time step
    while not done:

        #check to see wether or not to do a random action
        random_value = random.randrange(0,1)
        if random_value <= epsilon:
            action = env.action_space.sample()
        else:
            action = np.argmax(q_table[state])

        next_state, reward, done, info = env.step(action)
        q_table[state, action] += alpha * ( (reward + (np.max(q_table[next_state]))) - q_table[state, action])
        state = next_state

        if epsilon <= epsilon_min:
            epsilon = epsilon_min
        else:
            epsilon -= epsilon_decay

    if episode % 1000 == 0:
        try:
            os.remove("out-{}-episodes".format(episode - 1000))
        except Exception as e:
            pass

        print(f'Episode {episode}', end=': ')
        outfile_name = "out-{}-episodes".format(episode)
        outfile = open(outfile_name, "wb")
        np.save(outfile, q_table)
        outfile.close()
        print('saved')

print(f"Done, {EPISODES} episodes !")

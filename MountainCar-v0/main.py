import gym
import numpy as np
import random
import os
import math
# import cupy as cp

"""setup"""
env = gym.make("MountainCar-v0")
state = env.reset()
states_possible = 9999999
actions_possible = env.action_space.n
# q_dictionary = {}
EPISODES = range(0,9999999999999)
epsilon = 0.9
epsilon_min = 0.01
epsilon_decay = 0
alpha = 0.618
has_saved = True
in_file = open("out-25000-episodes", "rb")
# CUDA = ON
"""end_setup"""

if not has_saved:
    q_table = np.zeros([states_possible, actions_possible])
else:
    q_table = np.load(in_file)

first = True


# def run_episode(env, parameters):
#     observation = env.reset()
#     # observation = np.squeeze(observation)
#     totalreward = 0
#     for _ in range(200):
#         env.render()
#         if not first:
#             action = 0 if np.matmul(parameters,observation) < 0 else 1
#         else:
#             action = 0 if parameters[0] * observation[0] < 0 else 1
#         observation, reward, done, info = env.step(action)
#         totalreward += reward
#         if done:
#             break
#     print(parameters)
#     print(observation)
#     print(totalreward)
#     return totalreward



def cantor_pair(a,b):
    return int(np.rint(0.5 * (a + b) * (a + b + 1) + b))

print(cantor_pair(20, 7))

highest_reward = -200000
highest_higheset_reward = -200000

for episode in EPISODES:
    done = False
    reward = 0
    t_reward = 0
    state = env.reset()
    state = [0, 0]


    #do thing every time step
    # loop_count = 0
    while not done:

        # print(f"state-1 {state}")
        # loop_count += 1
        if episode % 1000 == 0:
            env.render()

        try:
            if state == 0:
                # print("dw")
                print(f"loop_count : {loop_count}")
                state = [0,0]
        except:
            pass
        # env.render()
        # print(f"state {state}")
        state = [int(state[0] * 1000) , int(state[1] * 1000) ]
        # print(f"state1 {state}")
        state = cantor_pair(state[0], state[1])
        # print(f"state2 {state}")
        #check to see wether or not to do a random action
        random_value = random.randrange(0,1)
        if random_value <= epsilon:
            action = env.action_space.sample()
        else:
            action = np.argmax(q_table[state])

        next_state, reward, done, info = env.step(action)
        # print(done)
        # print(f"statenext1 {next_state}")
        t_reward += reward
        next_state_p = next_state
        next_state = cantor_pair(next_state[0] * 1000, next_state[1] * 1000)
        # print(f"statenext2 {next_state}")
        # print(state)
        q_table[state, action] += alpha * ( (reward + (np.max(q_table[next_state]))) - q_table[state, action])

        # print(f"statenext3 {next_state}")
        state = next_state_p
        # print(f"state3 {state}")


    # print(t_reward)
    if highest_reward < t_reward:
        highest_reward = t_reward

    if highest_reward > highest_higheset_reward:
        highest_higheset_reward = highest_reward

    if episode % 1000 == 0:
        print(state)
        if epsilon <= epsilon_min:
            epsilon = epsilon_min
        else:
            epsilon -= epsilon_decay


        try:
            os.remove("out-{}-episodes".format(episode - 1000))
        except Exception as e:
            pass

        print(f'Episode {episode}', end=': ')
        outfile_name = "out-{}-episodes".format(episode)
        outfile = open(outfile_name, "wb")
        np.save(outfile, q_table)
        outfile.close()
        print('saved')
        print(f"Highest Reward: {highest_reward}")
        print(f"Highest highest Reward: {highest_higheset_reward}")
        print(np.nonzero(q_table))
        # for table in q_table:
        #     print(np.nonzero(table))

        highest_reward = -20000
